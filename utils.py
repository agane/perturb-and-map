'''
Utility functions for matchings/permutations operations with pytorch tensors
and variables.

Many of these functions are adapted from the tensorflow implementation (i.e.
copied & translated into pytorch).

https://github.com/google/gumbel_sinkhorn.
'''
from __future__ import print_function, division

import itertools
import math
import numpy as np
import os
import sys
import time
import torch
from torch.autograd import Variable

from PIL import Image
from torchvision import transforms

from scipy.optimize import linear_sum_assignment
from scipy.stats import kendalltau

sys.path.append('%s/../qpsolvers' % os.path.dirname(os.path.realpath(__file__)))
from qpsolvers import solve_qp

SOLVER = 'cvxopt'
# SOLVER = 'quadprog'
DIAG_EPS = 1e-3  # Value to add to the diagonal to ensure positive definiteness.
EQ_EPS = 1e-3  # Margin of error used for equality/inequality assertions.


def image_loader(file_name, imsize=256):
    ''' Load image, return pytorch tensor '''
    loader = transforms.Compose([transforms.Resize(imsize),
                                 transforms.ToTensor()])
    image = Image.open(file_name)
    image = loader(image).float()
    image = image.unsqueeze(0)
    return image


def scramble_batch(x_pieces):
    ''' Sample permutations (bz x num_pieces) & shuffle the pieces. '''
    perms = sample_permutations(x_pieces.size(0), x_pieces.size(1))
    if x_pieces.is_cuda:
        perms = perms.cuda()
    x_mixed_pieces = permute_batch_split(x_pieces, perms)
    return x_mixed_pieces, perms


def preprocess_batch(x_img, num_row_pieces):
    ''' Split image into pieces and create shuffled version.
    Args:
        x_img: 4D Tensor of shape (batch_size, num_channels, img_sz, img_sz)
            containing the input image. If 3D Tensor, then the input will be
            reshaped with batch_size = 1.
    Returns:
        x_pieces: 5D Tensor of shape (batch_size x num_pieces x num_channels x
            piece_sz, piece_sz) containing the pieces.
        x_mixed_pieces: 5D Tensor of the same shape as x_pieces containing the
            shuffled pieces.
        perms: 2D Tensor of size (batch_size, num_pieces) containing the
            permutations in listperm format.
    '''
    if x_img.dim() < 4:
        x_img = x_img.unsqueeze(0)
    x_pieces = image2pieces(x_img, num_row_pieces)
    x_mixed_pieces, perms = scramble_batch(x_pieces)
    return x_pieces, x_mixed_pieces, perms


def solve_puzzle(x_mixed_pieces, log_alpha=None):
    '''
    Args:
        x_pieces: 5D Tensor of shape (batch_size x num_pieces x num_channels x
            piece_sz, piece_sz) containing the shuffled pieces.
        log_alpha: predicted matrix
    Returns:
        x_pred_pieces: 5D Tensor of shape x_pieces.shape containing the
            re-ordered pieces (according to log_alpha)
    '''
    # The model predicts the permutation that shuffled the input
    perms_pred = matching(log_alpha)
    # To reconstruct the input, we compute and apply the inverse permutation
    inv_perms_pred = invert_listperm(perms_pred).contiguous()
    return permute_batch_split(x_mixed_pieces, inv_perms_pred)


def l2loss(x_pieces, x_mixed_pieces, soft_perms):
    ''' Loss used for training: L2 loss between (soft) reconstruction and
    the target image '''
    # Replicate the utils.invert_listperm
    inv_soft_perms = soft_perms.transpose(2, 3)
    inv_soft_perms_flat = inv_soft_perms.transpose(0, 1)
    inv_soft_perms_flat.view(-1, self.num_pieces, self.num_pieces)
    x_mixed_pieces_tiled = None # TODO
    x_pred_tiled = torch.bmm(inv_soft_perms_flat, x_mixed_pieces_tiled)
    x_pieces = None # TODO
    ordered_tiled = tf.reshape(self._ordered_tiled, [-1, self.n_numbers, 1])
    random_tiled = tf.reshape(self._random_tiled, [-1, self.n_numbers, 1])
    # squared l2 loss

    return torch.mean((x_img_repeat - torch.bmm(inv_soft_perms_flat, )).srq())
    self._l2s_diff = tf.reduce_mean(
        tf.square(
            ordered_tiled - tf.matmul(inv_soft_perms_flat, random_tiled)))


def compute_hard_losses(x_pieces, x_mixed_pieces, perms, log_alpha_w_noise):
    ''' Compute accuracy measures to be used by both methods (inverse
    optimization and gumbel softmax).
    Note that the log_alpha_w_noise contains multiple samples, as indicated by
    the second dimensions. Other inputs needs to be tiled/repeated/broadcast to
    match the size.

    Args:
        x_pieces: 5D Tensor of shape (batch_size x num_pieces x
            num_channels x piece_sz, piece_sz) containing the pieces.
        x_mixed_pieces: 5D Tensor of the same shape as x_pieces.
        perms: 2D Tensor of size (batch_size, num_pieces) containing the
            permutations in listperm format.
        log_alpha_w_noise: 4D Tensor of shape (batch_size, num_samples,
            num_pieces, num_pieces).
    Returns:
        dict: dictionary with pairs (loss_name, loss_value), which includes:
            - l1_diff (l1 norm between the reproduced image and target image),
            - l2_diff (l2 norm between the reproduced image and target image),
            - prop_wrong (overall prop of scrambled pieces that were wrongly assigned)
            - prop_any_wrong (prop of sequences where there was at least one error),
            - kendal_tau (the kendal tau metric)
    '''
    batch_size = log_alpha_w_noise.size(0)
    num_samples = log_alpha_w_noise.size(1)
    num_pieces = log_alpha_w_noise.size(-1)
    piece_sz = x_mixed_pieces.size(-1)
    # log_alpha_w_noise_flat contains multiple samples
    # (bz * num_samples) x num_pieces x num_pieces
    log_alpha_w_noise_flat = log_alpha_w_noise.view(-1, num_pieces, num_pieces)
    # so the predicted permutation also contains multiple samples
    # (bz * num_samples) x num_pieces
    perms_pred_flat = matching(log_alpha_w_noise_flat)
    # inversion ignores the match size, so the multiple samples don't matter
    # (bz * num_samples) x num_pieces
    inv_perms_pred_flat = invert_listperm(perms_pred_flat).contiguous()
    # repeat the pred, x_mixed_pieces, x_pieces to match the number of samples
    perms_tiled = perms.unsqueeze(1).repeat(1, num_samples, 1)
    perms_tiled_flat = perms_tiled.view(-1, num_pieces)
    x_mixed_pieces_tiled = x_mixed_pieces.unsqueeze(1).repeat(
            1, num_samples, 1, 1, 1, 1)
    x_mixed_pieces_tiled_flat = x_mixed_pieces_tiled.view(
            batch_size * num_samples, num_pieces, -1, piece_sz, piece_sz)
    if x_pieces is None:
        x_pieces_tiled_flat = permute_batch_split(x_mixed_pieces_tiled_flat, perms_tiled_flat)
    else:
        x_pieces_tiled = x_pieces.unsqueeze(1).repeat(1, num_samples, 1, 1, 1, 1)
        x_pieces_tiled_flat = x_pieces_tiled.view(x_mixed_pieces_tiled_flat.shape)
    x_pred_tiled_flat = permute_batch_split(x_mixed_pieces_tiled_flat,
                                            inv_perms_pred_flat)
    # Actual loss computation
    l1_diff = torch.mean(torch.abs(x_pieces_tiled_flat - x_pred_tiled_flat))
    l2_diff = torch.mean((x_pieces_tiled_flat - x_pred_tiled_flat) ** 2)
    diff_perms = torch.abs(perms_pred_flat - perms_tiled_flat)
    def mean(t):
        return t.sum() / t.view(-1).shape[0]
    prop_wrong = mean(diff_perms > 0)
    prop_any_wrong = mean(torch.max(diff_perms > 0, 1)[0])
    kendal_tau = np.mean(kendall_tau(perms_pred_flat, perms_tiled_flat))
    # Hamming loss on the binary results
    matperms_pred_flat = listperm2matperm(perms_pred_flat)
    matperms_tiled_flat = listperm2matperm(perms_tiled_flat)
    hamming = (1 - matperms_pred_flat) * matperms_tiled_flat + (1 - matperms_tiled_flat) * matperms_pred_flat
    hamming = mean(hamming.sum(2).sum(1))
    hard_losses = {'l1_diff':l1_diff, 'l2_diff':l2_diff, 'prop_wrong':prop_wrong,
                   'prop_any_wrong':prop_any_wrong, 'kendal_tau':kendal_tau,
                   'hamming':hamming}
    return hard_losses


def gaussian_nll(val, mu, logvar):
    ''' Compute the negative log likelihood for a set of gaussian variables.

    Args:
        val: A Variable (a batch of assignments).
        mu: A Variable (a batch of means).
        logvar: A Variable (a batch of logvars). The args can have any shape
            as long as they match.
    Returns:
        nll: One negative log likelihood value for Normal(mean, var) at the
            value at the corresponding position.
    '''
    assert val.data.shape == mu.data.shape
    assert val.data.shape == logvar.data.shape
    var = logvar.exp()
    logstd = 0.5 * logvar
    shift = math.log(math.sqrt(2 * math.pi))
    nll = ((val - mu) ** 2) / (2 * var.clamp(min=1e-10)) + logstd + shift
    return nll

def gaussian_nll_loss(val, mu, logvar, size_average=True, option=1):
    ''' Compute the negative log likelihood for a set of gaussian variables.

    Args:
        val: A Variable (a batch of assignments).
        mu: A Variable (a batch of means).
        logvar: A Variable (a batch of logvars).
    Returns:
        loss: the loss value corresponding to the negative log likelihood,
            similar to other losses in pytorch: average over all the dimensions,
            batch or otherwise (when size_average = True) or sum (when
            size_average = False).
    '''
    if option == 1:
        normal = torch.distributions.Normal(mu, (0.5 * logvar).exp())
        nll_values = - normal.log_prob(val)
    elif option == 2:
        nll_values = gaussian_nll(val, mu, logvar)
    else:
        normal = torch.distributions.Normal(mu, (0.5 * logvar).exp())
        nll_values_1 = - normal.log_prob(val)
        nll_values_2 = gaussian_nll(val, mu, logvar)
        difference = (nll_values_1 - nll_values_2).abs().sum().data[0]
        assert difference < EQ_EPS, difference
        nll_values = nll_values_2
    if size_average:
        return torch.mean(nll_values)
    return torch.sum(nll_values)

def image2pieces(image_batch, n_pieces):
    ''' Split image into n_pieces x n_pieces square pieces of the same size.

    Args:
        image_batch: A 4D tensor (a batch of images) with shape
            [batch_size, channel_size, image_size, image_size]. If 3D, the input
            is reshaped to 4D with batch_size = 1.
        n_pieces: indicates the number of pieces to split in on each axis (rows
            and cols).
    Returns:
        pieces_batch: A 5D tensor (a batch of pieces) with shape
            [batch_size, n_pieces ** 2, channel_size, image_size, image_size]
    '''
    if image_batch.dim() == 3:
        image_batch = image_batch.unsqueeze(0)
    piece_sz = image_batch.size(-1) // n_pieces
    new_img_sz = piece_sz * n_pieces
    image_batch = image_batch[:, :, 0:new_img_sz, 0:new_img_sz].contiguous()
    pieces_batch = image_batch.view(image_batch.size(0), image_batch.size(1),
                                    n_pieces, piece_sz, n_pieces, piece_sz)
    pieces_batch = pieces_batch.permute(0, 2, 4, 1, 3, 5)
    pieces_batch = pieces_batch.contiguous()
    pieces_batch = pieces_batch.view(image_batch.size(0), n_pieces ** 2,
                                     image_batch.size(1), piece_sz, piece_sz)
    return pieces_batch

def pieces2image(pieces_batch):
    ''' Gather image from pieces.

    Args:
        pieces_batch: A 5D tensor (a batch of pieces) with shape
            [batch_size, n_pieces ** 2, channel_size, image_size, image_size].
            If 4D, the input is reshaped to 5D with batch_size = 1.
    Returns:
        image_batch: A 4D tensor (a batch of images) with shape
            [batch_size, channel_size, image_size, image_size].
    '''
    if pieces_batch.dim() == 4:
        pieces_batch = pieces_batch.unsqueeze(0)
    n_pieces = int(math.sqrt(pieces_batch.size(1)))
    piece_sz = pieces_batch.size(-1)
    img_sz = piece_sz * n_pieces
    pieces_batch = pieces_batch.view(pieces_batch.size(0), n_pieces, n_pieces,
                                     pieces_batch.size(2), piece_sz, piece_sz)
    pieces_batch = pieces_batch.permute(0, 3, 1, 4, 2, 5)
    pieces_batch = pieces_batch.contiguous()
    image_batch = pieces_batch.view(pieces_batch.size(0), pieces_batch.size(1),
                                    img_sz, img_sz)
    return image_batch

def matching(matrix_batch):
    ''' Solves a matching problem for a batch of matrices

    This is a wrapper for the scipy.optimize.linear_sum_assignment function.
    It solves the optimization problem max_P sum_i,j M_i,j P_i,j with P a
    permutation matrix. Notice the negative sign; the reason, the original
    function solves a minimization problem.

    Args:
      	matrix_batch: A 3D tensor (a batch of matrices) with
            shape = [batch_size, N, N]. If 2D, the input is reshaped to 3D with
	    batch_size = 1.
    Returns:
        listperms, a 2D integer tensor of permutations with shape
            [batch_size, N] so that listperms[n, :] is the permutation of
            range(N) that solves the problem  max_P sum_i,j M_i,j P_i,j with
            M = matrix_batch[n, :, :].
    '''
    def hungarian(x):
	if x.ndim == 2:
	    x = np.reshape(x, [1, x.shape[0], x.shape[1]])
	sol = np.zeros((x.shape[0], x.shape[1]), dtype=np.int32)
	for i in range(x.shape[0]):
	    sol[i, :] = linear_sum_assignment(-x[i, :])[1].astype(np.int32)
	return sol

    return torch.LongTensor(hungarian(matrix_batch.cpu().numpy()))

def inverse_matching(listperm, mu, var, margin=True, margin_scale=1,
                     verbose=True, use_unnecessary_checks=True):
    ''' Solves an inverse matching problem.

    We assume a gaussian distribution over latent variables so the inverse
    problem is a quadratic program. This function is a wrapper for Python's
    quadprog/cvxopt solvers.
    We are given a distribution over latent variables p(w; mu, logvar) s.t. the
    model samples are computed via: w ~ p(w; mu, logvar), m_out = matching(w).
    We return the assignment w = argmax_w [log p(w; mu, logvar) s.t.
    m = matching(w)] where m = listperm(i), mu = mu[i], logvar = logvar[i].

    Args:
        listperm: 2D tensor of permutations of shape [batch_size, n_objects] so
            that listperm[n] is a permutation of range(n_objects). If the input
            is 1D, it is reshaped to 2D with batch_size = 1.
        mu: 3D tensor of mean variables of shape [batch_size, n_objects,
            n_objects]. If the input is 2D, it is reshaped to 3D with
            batch_size = 1.
        var: 3D tensor of variances of shape [batch_size, n_objects,
            n_objects]. If the input is 2D, it is reshaped to 3D with
            batch_size = 1.
    Returns:
        latents: 3D tensor of latent variables assignments of shape
            [batch_size, n_objects, n_objects]. We should have that
            matching(latents) = listperm.
    '''
    def evaluate_perm(listperm, latent):
        print('\033[93mUsing an unnecessary check: evaluate_perm\033[0m')
        listperm = listperm.cpu()
        new_listperm = matching(latent)
        def score_perm(listperm):
            score_target = listperm2matperm(listperm).mul(latent)
            return score_target.sum(1).sum(1)
        if verbose: print('--- Evaluate permutation ---')
        if verbose: print('w', latent.numpy())
        if verbose: print('hat{p}', listperm.numpy(), score_perm(listperm).numpy()[0])
        if verbose: print('p', new_listperm.numpy(), score_perm(new_listperm).numpy()[0])
        score_change = (score_perm(listperm) - score_perm(new_listperm))
        score_change = score_change.abs().sum() / listperm.size(0)
        assert score_change < EQ_EPS, score_change
        perm_change = (listperm - new_listperm).abs().gt(0).sum(1).gt(0).sum()
        if verbose: print('Inversion accuracy %.2f%%'
                % ((1 - perm_change / listperm.size(0)) * 100))

    def evaluate_feasibility(A, b, G, h, x, verbose=False):
        ''' Check whether A*x = b, G*x <= h '''
        print('\033[93mUsing an unnecessary check: evaluate_feasibility\033[0m')
        if verbose:
            if A is not None: print(A.shape)
            if b is not None: print(b.shape)
        if A is not None:
            lhs = np.matmul(A, x)
            loss = abs(lhs - b).sum()
            assert loss < EQ_EPS, 'eq constraints not statisfied %f' % loss
        lhs = np.matmul(G, x)   # lhs <= h + EPS
        loss = (h - lhs + EQ_EPS) >= 0
        assert np.all(loss), 'ineq constraints not satisfied'

    def check_solver_requirements(objective_p, objective_q, A, B, G, H, verbose=False):
        print('\033[93mUsing an unnecessary check: check_solver_requirements\033[0m')
        if SOLVER == 'cvxopt':
            if A is not None:
                rank = np.linalg.matrix_rank(A)
                assert rank == A.shape[0]
                T = np.concatenate([objective_p, A.transpose(), G.transpose()], axis=1)
            else:
                T = np.concatenate([objective_p, G.transpose()], axis=1)
            rank = np.linalg.matrix_rank(T)
            if verbose and rank != T.shape[0]:
                print('T', T)
            assert rank == T.shape[0], 'rank=%d / %d' % (rank, T.shape[0])
        elif SOLVER == 'quadprog':
            # Check that the problem is strictly convex
            assert np.all(np.linalg.eigvals(objective_p) > 0)

    def run_quadprog_solver(matperm, mu, var, margin, margin_scale, verbose):
        if matperm.ndim == 1:
            matperm = np.reshape(matperm, [1, matperm.shape[0]])
        if mu.ndim == 1:
            mu = np.reshape(mu, [1, mu.shape[0]])
        if var.ndim == 1:
            var = np.reshape(var, [1, var.shape[0]])
        assert var.min() >= 0, 'Did you pass logvar accidentally?'
        # Count the elements
        n_objects = mu.shape[-1]
        n_objects_sq = n_objects ** 2
        n_duals = n_objects * 2
        n_vars = n_objects_sq + n_duals
        if verbose:
            if verbose: print('objects %d, cost vars %d, total vars %d' %
                  (n_objects, n_objects_sq, n_vars))
        # We need these in flat form.
        matperm_flat = matperm.reshape(matperm.shape[0], -1)
        mu_flat = mu.reshape(mu.shape[0], -1)
        inv_var_flat = (1 / var).reshape(var.shape[0], -1)

        def dual_constraint_columns(which_var):
            ''' This constructs columns for the constraint matrix in the LHS
                of the dual feasibility constraints:
                u_i + v_j >= w_{i,j} + margin * (e - 2 hat{x})
                which we rewrite as follows (since quadprog wants Gx <= h)
                w_{i,j} - u_i - v_j <= margin * (e - 2 hat{x})
                Args:
                    which_var: scalar indicating which dual variable group we
                        are building the constraint matrix columns form:
                        which_var = 0 indicates columns for u and
                        which_var != 0 indicates columns for v
            '''
            out = np.zeros((n_objects, n_objects, n_objects))
            for i in range(n_objects):
                if which_var == 0:
                    out[i, :, i] = -1
                else:
                    out[:, i, i] = -1
            out = out.reshape((n_objects_sq, n_objects))
            return out
        ind_costvar = np.eye(n_objects_sq)
        ind_duals_0 = dual_constraint_columns(which_var=0)
        ind_duals_1 = dual_constraint_columns(which_var=1)
        constraints_lhs = np.concatenate([ind_costvar,
                                          ind_duals_0,
                                          ind_duals_1], 1)
        # Other terms (constraint matrices, cost matrices) depend on the
        # particular instance (on hat{x}, mu or sigma) so we set them to 0 and
        # update. inside the loop.
        constraints_rhs = np.zeros(constraints_lhs.shape[0])
        if margin:
            # Add a placeholder for the objects inequality constraint:
            # w * hat{x} - e * hat{x} >= e * u + e * v
            # which we rewrite as follows (since quadprog wants Gx <= h)
            # - w * hat{x} + e * u + e * v <= - e * hat{x}
            cost_lhs = np.zeros((1, constraints_lhs.shape[1]))
            cost_rhs = np.zeros(1).reshape(1)
            constraints_lhs = np.concatenate([constraints_lhs, cost_lhs], 0)
            constraints_rhs = np.concatenate([constraints_rhs, cost_rhs], 0)
        objective_p = np.zeros((n_vars, n_vars))
        objective_q = np.zeros((n_vars))
        batch_solution = np.zeros(mu.shape)
        for i in range(listperm.shape[0]):
            if margin:
                # Update the constraints which depend on the current instance.
                constraints_lhs[-1, 0:n_objects_sq] = - matperm_flat[i]
                constraints_lhs[-1, n_objects_sq:] = 1
                constraints_rhs[0:n_objects_sq] = margin_scale * (matperm_flat[i] * 2 - 1)
                constraints_rhs[-1] = margin_scale * (- matperm[i].sum())
                A = None
                b = None
                G = constraints_lhs
                h = constraints_rhs
            else:
                # Extract equality constraints (Ax=b) from to the rows of
                # constraint matrices constraints_{lhs, rhs} where matperm = 1,
                # and inequality constraints (Gx<=h) from the rows where
                # matperm = 0.
                ones = matperm_flat[i].astype(bool)
                A = constraints_lhs[ones]
                b = constraints_rhs[ones]
                G = constraints_lhs[~ones]
                h = constraints_rhs[~ones]
            # Update the objective matrices
            diag = inv_var_flat[i] - DIAG_EPS
            objective_p[0:n_objects_sq, 0:n_objects_sq] = np.diag(diag)
            objective_p += np.eye(n_vars, n_vars) * DIAG_EPS
            objective_q[0:n_objects_sq] = - mu_flat[i] * inv_var_flat[i]
            if verbose: print('P', objective_p.shape, objective_p)
            if verbose: print('q', objective_q.shape, objective_q)
            if verbose and A is not None: print('A', A.shape, A)
            if verbose and b is not None: print('b', b.shape, b)
            if verbose: print('G', G.shape, G.transpose())
            if verbose: print('h', h.shape, h)
            # Solve min (1/2)x'Px + q'x s.t. Gx <= h, Ax = b.
            if use_unnecessary_checks:
                check_solver_requirements(objective_p, objective_q, A, b, G, h)
            x = solve_qp(P=objective_p, q=objective_q,
                         G=G, h=h, A=A, b=b, solver=SOLVER)
            if x is None:
                print('QP solver did not find optimal solution')
                return None

            if use_unnecessary_checks:
                evaluate_feasibility(A, b, G, h, x)
            batch_solution[i, :, :] = x[0:n_objects ** 2].reshape((n_objects,
                                                                   n_objects))
        return batch_solution

    if verbose:
        print('--- Inverse Optimization ---')
        print('listperm', listperm.numpy())
        print('mean', mu.numpy())
        print('var', var.numpy())
        print('margin', margin, margin_scale)
    matperm = listperm2matperm(listperm)
    if matperm.is_cuda:
        matperm = matperm.cpu()
    if mu.is_cuda:
        mu = mu.cpu()
    if var.is_cuda:
        var = var.cpu()
    latent = run_quadprog_solver(matperm.numpy(), mu.numpy(), var.numpy(),
                                 margin=margin, margin_scale=margin_scale,
                                 verbose=verbose)
    if latent is None:
        return None
    latent = torch.Tensor(latent)
    if use_unnecessary_checks:
        evaluate_perm(listperm, latent)
    return latent


def kendall_tau(batch_perm1, batch_perm2):
    ''' Wraps scipy.stats kendalltau function.
    Args:
        batch_perm1: A 2D tensor (a batch of matrices) with
            shape = [batch_size, N]
        batch_perm2: same as batch_perm1
    Returns:
        An array of Kendall distances between each of the elements of the batch.
    '''
    def kendalltau_batch(x, y):
        if x.ndim == 1:
            x = np.reshape(x, [1, x.shape[0]])
        if y.ndim == 1:
            y = np.reshape(y, [1, y.shape[0]])
        kendall = np.zeros((x.shape[0], 1), dtype=np.float32)
        for i in range(x.shape[0]):
            kendall[i, :] = kendalltau(x[i, :], y[i, :])[0]
        return kendall

    return kendalltau_batch(batch_perm1.cpu().numpy(),
                            batch_perm2.cpu().numpy())

def sample_permutations(n_permutations, n_objects, option=2, verbose=False):
    '''Samples a batch permutations from the uniform distribution.
    Returns a sample of n_permutations permutations of n_objects indices.
    Permutations are assumed to be represented as lists of integers
    (see 'listperm2matperm' and 'matperm2listperm' for conversion to alternative
    matricial representation). It does so by sampling from a continuous
    distribution and then ranking the elements. By symmetry, the resulting
    distribution over permutations must be uniform.
    Args:
        n_permutations: An int, the number of permutations to sample.
        n_objects: An int, the number of elements in the permutation.
    Returns:
        A 2D integer tensor with shape [n_permutations, n_objects], where each
        row is a permutation of range(n_objects)
    '''
    if verbose: tic = time.time()
    if option == 1:
        random_pre_perm = torch.normal(torch.zeros(n_permutations, n_objects),
                                       torch.ones(n_permutations, n_objects))
        _, permutations = torch.sort(random_pre_perm, 1)
    else:
        permutations = [np.random.permutation(n_objects)
                        for i in range(n_permutations)]
        permutations = np.stack(permutations)
        permutations = torch.LongTensor(permutations)
    if verbose: print('time', time.time() - tic)

    return permutations

def permute_batch_split(batch_split, permutations):
    ''' Scrambles a batch of objects according to permutations.
    It takes a 3D tensor [batch_size, n_objects, object_size]
    and permutes items in axis=1 according to the 2D integer tensor
    permutations, (with shape [batch_size, n_objects]) a list of permutations
    expressed as lists. For many dimensional-objects (e.g. images), objects have
    to be flattened so they will respect the 3D format, i.e. tf.reshape(
    batch_split, [batch_size, n_objects, -1])
    Args:
        batch_split: 3D tensor with shape = [batch_size, n_objects, object_size] of
        splitted objects
        permutations: a 2D integer tensor with shape = [batch_size, n_objects] of
        permutations, so that permutations[n] is a permutation of range(n_objects)
    Returns:
        A 3D tensor perm_batch_split with the same shape as batch_split,
        so that perm_batch_split[n, j,:] = batch_split[n, perm[n,j],:]
    '''
    batch_size = batch_split.size(0)
    n_objects = batch_split.size(1)
    ind_permutations = permutations.view(-1, 1)
    ind_batch = torch.arange(batch_size).view(-1, 1)
    ind_batch = ind_batch.repeat(1, n_objects).view(-1, 1).long()
    gather = torch.stack([batch_split[i[0],j[0]]
                          for (i,j) in zip(ind_batch, ind_permutations)])
    batch_split = gather.view(batch_split.size())
    return batch_split


def listperm2matperm(listperm):
    ''' Converts a batch of permutations to its matricial form.
    Args:
        listperm: 2D tensor of permutations of shape [batch_size, n_objects] so
            that listperm[n] is a permutation of range(n_objects). If the input
            is 1D, it is reshaped to 2D with batch_size = 1.
    Returns:
        a 3D tensor of permutations matperm of shape = [batch_size, n_objects,
        n_objects] so that matperm[n, :, :] is a permutation of the identity
        matrix, with matperm[n, i, listperm[n,i]] = 1
    '''
    if listperm.dim() == 1:
        listperm = listperm.unsqueeze(0)
    n_objects = listperm.size(1)
    if listperm.is_cuda:
        return torch.stack([
                torch.cuda.FloatTensor(n_objects, n_objects).fill_(0).scatter_(1, perm, 1)
                for perm in listperm.unsqueeze(-1)])
    else:
        return torch.stack([
                torch.zeros(n_objects, n_objects).scatter_(1, perm, 1)
                for perm in listperm.unsqueeze(-1)])

def matperm2listperm(matperm, dtype=np.int32):
    ''' Converts a batch of permutations to its enumeration (list) form.
    Args:
        matperm: a 3D tensor of permutations of
            shape = [batch_size, n_objects, n_objects] so that matperm[n, :, :]
            is a permutation of the identity matrix. If the input is 2D, it is
            reshaped to 3D with batch_size = 1.
        dtype: output_type (tf.int32, tf.int64)
    Returns:
        A 2D tensor of permutations listperm, where listperm[n,i]
        is the index of the only non-zero entry in matperm[n, i, :]
    '''
    if matperm.dim() == 2:
        matperm = matperm.unsqueeze(0)
    batch_size = matperm.size(0)
    n_objects = matperm.size(1)
    _, inds = torch.sort(matperm, 2)
    return inds[:, :, -1]

def invert_listperm(listperm):
    '''Inverts a batch of permutations.
    Args:
        listperm: a 2D integer tensor of permutations listperm of
            shape = [batch_size, n_objects] so that listperm[n] is a
            permutation of range(n_objects)
    Returns:
        A 2D tensor of permutations listperm, where listperm[n,i]
            is the index of the only non-zero entry in matperm[n, i, :]
    '''
    return matperm2listperm(listperm2matperm(listperm).transpose(1,2))

# More functions needed to implement Gumbel-Sinkhorn in pytorch

def sample_gumbel(shape, eps=1e-20):
    '''Samples arbitrary-shaped standard gumbel variables.
    Args:
	shape: list of integers
	eps: float, for numerical stability
    Returns:
	A sample of standard Gumbel random variables
    '''
    u = torch.Tensor(torch.Size(shape)).uniform_(0, 1)
    return -torch.log(-torch.log(u + eps) + eps)


def logsumexp(inputs, dim=None, keepdim=False):
    '''Numerically stable logsumexp.
    From https://github.com/pytorch/pytorch/issues/2591
    Args:
        inputs: A Variable with any shape.
        dim: An integer.
        keepdim: A boolean.

    Returns:
        Equivalent of log(sum(exp(inputs), dim=dim, keepdim=keepdim)).
    '''
    # For a 1-D array x (any array along a single dimension),
    # log sum exp(x) = s + log sum exp(x - s)
    # with s = max(x) being a common choice.
    if dim is None:
        inputs = inputs.view(-1)
        dim = 0
    s, _ = torch.max(inputs, dim=dim, keepdim=True)
    outputs = s + (inputs - s).exp().sum(dim=dim, keepdim=True).log()
    if not keepdim:
        outputs = outputs.squeeze(dim)
    return outputs


def sinkhorn(log_alpha, n_iters=20):
    '''Performs incomplete Sinkhorn normalization to log_alpha.

    By a theorem by Sinkhorn and Knopp [1], a sufficiently well-behaved matrix
    with positive entries can be turned into a doubly-stochastic matrix
    (i.e. its rows and columns add up to one) via the succesive row and column
    normalization.

    - To ensure positivity, the effective input to sinkhorn has to be
    exp(log_alpha) (elementwise).

    - However, for stability, sinkhorn works in the log-space. It is only at
    return time that entries are exponentiated.
    [1] Sinkhorn, Richard and Knopp, Paul.
    Concerning nonnegative matrices and doubly stochastic
    matrices. Pacific Journal of Mathematics, 1967

    Args:
	log_alpha: 2D tensor (a matrix of shape [N, N])
	or 3D tensor (a batch of matrices of shape = [batch_size, N, N])
	n_iters: number of sinkhorn iterations (in practice, as little as 20
	iterations are needed to achieve decent convergence for N~100)
    Returns:
	A 3D tensor of close-to-doubly-stochastic matrices (2D tensors are
	converted to 3D tensors with batch_size equals to 1)
    '''
    log_alpha = log_alpha.clone()
    if log_alpha.dim() == 2:
        log_alpha = log_alpha.unsqueeze(0)
    n = log_alpha.size(-1)
    for _ in range(n_iters):
	log_alpha -= logsumexp(log_alpha, dim=2).view(-1, n, 1)
	log_alpha -= logsumexp(log_alpha, dim=1).view(-1, 1, n)
    return torch.exp(log_alpha)


def add_noise(log_alpha, temp=1.0, n_samples=1, noise_factor=1.0,
              distribution='gaussian', gaussian_var=0.5):
    '''
    Creates the noisy log_alpha.

    Args:
	log_alpha: 2D tensor (a matrix of shape [N, N])
            or 3D tensor (a batch of matrices of shape = [batch_size, N, N])
	temp: temperature parameter, a float.
	n_samples: number of samples
	noise_factor: scaling factor for the gumbel samples. Mostly to explore
            different degrees of randomness (and the absence of randomness, with
            noise_factor=0)
        distribution: gaussian/gumbel for the noise
        gaussian_var: in case it's gaussian, use this variance
    '''
    log_alpha = log_alpha.clone()
    if log_alpha.dim() == 2:
        log_alpha = log_alpha.unsqueeze(0)
    n = log_alpha.size(-1)
    batch_size = log_alpha.size(0)
    log_alpha_w_noise = log_alpha.view(1, batch_size, n, n)
    log_alpha_w_noise = log_alpha_w_noise.repeat(n_samples, 1, 1, 1)
    log_alpha_w_noise = log_alpha_w_noise.view(n_samples * batch_size, n, n)
    if noise_factor == 0:
	noise = 0.0
    else:
        if distribution == 'gumbel':
            noise = sample_gumbel([n_samples * batch_size, n, n]) * noise_factor
        elif distribution == 'gaussian':
            noise = torch.Tensor(torch.Size([n_samples * batch_size, n, n]))
            noise = noise.normal_(mean=0, std=math.sqrt(gaussian_var)) * noise_factor
        else:
            raise NotImplementedError
    log_alpha_w_noise += noise
    log_alpha_w_noise /= temp
    return log_alpha_w_noise


def random_sinkhorn(log_alpha,
                    temp=1.0, n_samples=1, noise_factor=1.0, n_iters=20,
                    squeeze=True, distribution='gumbel', gaussian_var=0.5):
    '''Random doubly-stochastic matrices via gumbel noise.

    In the zero-temperature limit sinkhorn(log_alpha/temp) approaches
    a permutation matrix. Therefore, for low temperatures this method can be
    seen as an approximate sampling of permutation matrices, where the
    distribution is parameterized by the matrix log_alpha

    The deterministic case (noise_factor=0) is also interesting: it can be
    shown that lim t->0 sinkhorn(log_alpha/t) = M, where M is a
    permutation matrix, the solution of the
    matching problem M=arg max_M sum_i,j log_alpha_i,j M_i,j.

    Therefore, the deterministic limit case of random_sinkhorn can be seen
    as approximate solving of a matching problem, otherwise solved via the
    Hungarian algorithm.

    Warning: the convergence holds true in the limit case n_iters = infty.
    Unfortunately, in practice n_iter is finite which can lead to numerical
    instabilities, mostly if temp is very low. Those manifest as
    pseudo-convergence or some row-columns to fractional entries (e.g.
    a row having two entries with 0.5, instead of a single 1.0)

    To minimize those effects, try increasing n_iter for decreased temp.
    On the other hand, too-low temperature usually lead to high-variance in
    gradients, so better not choose too low temperatures.

    Args:
	log_alpha: 2D tensor (a matrix of shape [N, N])
            or 3D tensor (a batch of matrices of shape = [batch_size, N, N])
	temp: temperature parameter, a float.
	n_samples: number of samples
	noise_factor: scaling factor for the gumbel samples. Mostly to explore
            different degrees of randomness (and the absence of randomness, with
            noise_factor=0)
	n_iters: number of sinkhorn iterations. Should be chosen carefully, in
            inverse corresponde with temp to avoid numerical stabilities.
            squeeze: a boolean, if True and there is a single sample, the output
            will remain being a 3D tensor.
    Returns:
	sink: a 4D tensor of [batch_size, n_samples, N, N] i.e.
            batch_size *n_samples doubly-stochastic matrices. If n_samples = 1 and
            squeeze = True then the output is 3D.
	log_alpha_w_noise: a 4D tensor of [batch_size, n_samples, N, N] of
            noisy samples of log_alpha, divided by the temperature parameter. If
            n_samples = 1 then the output is 3D.
    '''
    log_alpha_w_noise = add_noise(log_alpha,
                                  temp=temp,
                                  n_samples=n_samples,
                                  noise_factor=noise_factor,
                                  distribution=distribution,
                                  gaussian_var=gaussian_var)
    sink = sinkhorn(log_alpha_w_noise, n_iters)
    if n_samples > 1 or squeeze is False:
	sink = sink.view(n_samples, batch_size, n, n)
        sink = sink.transpose(0, 1)
	log_alpha_w_noise = log_alpha_w_noise.view(n_samples, batch_size, n, n)
        log_alpha_w_noise = log_alpha_w_noise.transpose(0, 1)

    return sink, log_alpha_w_noise
