''' Training script '''
from __future__ import print_function, division

import os
import argparse
import numpy as np
import random
import time
import torch
from torch.autograd import Variable
import torch.optim as optim
from torchvision import datasets, transforms
from tqdm import tqdm

import sklearn
from sklearn.manifold import TSNE

import matplotlib
matplotlib.use('Agg')
import matplotlib.pyplot as plt

from data_loader import get_train_valid_loader, get_test_loader
from visdom_utils import show_images, start_visdom, update_plot
from models import InverseOptimizationNet, RandomSinkhornNet
import utils


def add_point_to_plot(viz_dict, label, x, y):
    ''' Provides an one-liner for updatting the visdom training curve plot. '''
    win = 'win_' + label
    if not win in viz_dict:
        viz_dict[win] = None
    viz_dict[win], viz_dict['viz_env'] = update_plot(
            X=np.array([x]).reshape(1),
            Y=np.array([y]).reshape(1),
            label=label,
            win=viz_dict[win],
            viz=viz_dict['viz_env'])


def visualize_latent_space(data, labels, figname=None):
    vis_data = TSNE(n_components=2).fit_transform(data)
    vis_x = vis_data[:, 0]
    vis_y = vis_data[:, 1]
    plt.scatter(vis_x, vis_y, c=labels[:, 0], cmap=plt.cm.get_cmap("jet", 10))
    plt.colorbar(ticks=range(10))
    plt.clim(-0.5, 9.5)
    plt.savefig(figname + '.png')

def visualize_jigsaw_solving(x_mixed_pieces, log_alpha=None, label=None, x_pieces=None, display_mixed_pieces=True):
    '''
    Args:
        x_pieces: 5D Tensor of shape (batch_size x num_pieces x num_channels x
            piece_sz, piece_sz) containing the pieces.
        label: String to be used as visdom pane title or file name. E.g.
            'demo/unscramble_m=%s' % str(margin).lower()
    Returns:
        No value. Shows images in visdom.
    '''
    if display_mixed_pieces:
        disp_list = [utils.pieces2image(x_mixed_pieces.data)]
    else:
        disp_list = []
    if log_alpha is not None:
        x_pred_pieces = utils.solve_puzzle(x_mixed_pieces, log_alpha)
        disp_list.append(utils.pieces2image(x_pred_pieces.data))
    if x_pieces is not None:
        disp_list.append(utils.pieces2image(x_pieces.data))
    for k in disp_list:
        print(k.shape)
    disp = torch.cat(disp_list, dim=0)
    show_images(disp, nrow=x_mixed_pieces.size(0), save_png=True, title=label)


def setup_optimizer(args, parameters):
    if args.optimizer == 'adam':
        return optim.Adam(parameters, lr=args.lr,
                          weight_decay=args.weight_decay)
    elif args.optimizer == 'sgd':
        return optim.SGD(parameters, lr=args.lr,
                         momentum=args.momentum, weight_decay=args.weight_decay)
    print('Unknown optimizer %s' % args.optimizer)
    exit()


def setup_model(args):
    if args.model_type == 'inv-opt':
        return InverseOptimizationNet(num_row_pieces=args.num_row_pieces,
                                      gaussian_var=args.gaussian_var,
                                      margin=not args.no_margin,
                                      margin_scale=args.margin_scale,
                                      img_sz=args.resolution)
    elif args.model_type == 'gumbel-sinkhorn':
        return RandomSinkhorn(num_row_pieces=args.num_row_pieces,
                              samples_per_num=args.samples_per_num,
                              n_iter_sinkhorn=args.n_iter_sinkhorn,
                              noise_factor=args.noise_factor,
                              img_sz=args.resolution)
    elif args.model_type == 'gaussian-sinkhorn':
        return RandomSinkhorn(num_row_pieces=args.num_row_pieces,
                              gaussian_var=args.gaussian_var,
                              samples_per_num=args.samples_per_num,
                              n_iter_sinkhorn=args.n_iter_sinkhorn,
                              noise_factor=args.noise_factor,
                              img_sz=args.resolution)
    print('Model not found %s' % args.model_type)
    exit()


def train(args, model, use_cuda, train_loaders, optimizer, epoch, viz_dict):
    train_loader, valid_loader = train_loaders
    count_skipped = 0
    count_total = 0
    for batch_idx, (data, target) in enumerate(train_loader):
        global_iter = (epoch - 1) * len(train_loader) + batch_idx
        if args.eval_every > 0 and batch_idx % args.eval_every == 0:
            test(args, model, use_cuda, valid_loader, global_iter, viz_dict)
            # Save model checkpoint
            if args.save_dir:
                fname = ('%s/%s_%d.dat' % (args.save_dir, args.model_name,
                                           global_iter))
                torch.save(model.state_dict(), fname)

        model.train()
        tic = time.time()
        x_pieces, x_mixed_pieces, perms = utils.preprocess_batch(
                data, args.num_row_pieces)
        x_mixed_pieces = Variable(x_mixed_pieces)
        if use_cuda:
            x_pieces = x_pieces.cuda()
            x_mixed_pieces = x_mixed_pieces.cuda()
        optimizer.zero_grad()
        outs = model(x_mixed_pieces, perms)
        if outs is None:
            count_skipped += 1
            continue
        count_total += 1
        loss, acc, mean_pot, posterior_pot = outs
        if args.visualize:
            visualize_jigsaw_solving(x_mixed_pieces, mean_pot.data, 'train_batch')
            raw_input("Press Enter to continue...")
        loss.backward()
        optimizer.step()
        if batch_idx % args.log_every == 0:
            print('Train Epoch %d [%d/%d (%.0f%%)][%.2fs]\tLoss %.6f\tExp %.6f'
                    % (epoch, batch_idx * len(data), len(train_loader.dataset),
                       100. * batch_idx / len(train_loader), time.time() - tic,
                       loss.data[0], (-loss).exp().data[0]),
                    '\t'.join(['%s %.4f' % (key, acc[key]) for key in acc]))
            viz_dict['count'] += 1
            add_point_to_plot(viz_dict, 'loss', viz_dict['count'], loss.data[0])
            for key in acc:
                add_point_to_plot(viz_dict, key, viz_dict['count'], acc[key])

    print('Count skipped:', count_skipped,
            'Percentage skipped:', count_skipped / count_total)
    test(args, model, use_cuda, valid_loader, global_iter, viz_dict)

    return count_skipped, count_total

def test(args, model, use_cuda, loader, global_iter, viz_dict, noise_factor=1):
    model.eval()
    accum_losses = {}
    count = 0
    pbar = tqdm(total=len(loader))
    for batch_idx, (data, target) in enumerate(loader):
        x_pieces, x_mixed_pieces, perms = utils.preprocess_batch(
                data, args.num_row_pieces)
        x_pieces = Variable(x_pieces)
        x_mixed_pieces = Variable(x_mixed_pieces)
        if use_cuda:
            x_pieces = x_pieces.cuda()
            x_mixed_pieces = x_mixed_pieces.cuda()
        new_losses = model.evaluate(x_pieces, x_mixed_pieces, perms)
        for key in new_losses:
            if key not in accum_losses:
                accum_losses[key] = 0
            new_loss = new_losses[key]
            batch_size = x_pieces.size(0)
            accum_losses[key] += new_loss * batch_size
        count += x_pieces.size(0)
        pbar.update(1)
    pbar.close()

    for key in accum_losses:
        accum_losses[key] /= count
    print('Evaluation Losses',
          '\t'.join(['%s %.4f' % (key, accum_losses[key])
                     for key in accum_losses]))
    if viz_dict is not None:
        # Write values to file
        viz_dict['valid_losses_file'].write('%s,%s,%s,%s,%s,%s\n' % (
            accum_losses['l1_diff'], accum_losses['l2_diff'],
            accum_losses['kendal_tau'], accum_losses['hamming'],
            accum_losses['prop_wrong'], accum_losses['prop_any_wrong']
            ))
        # Add to plots
        viz_dict['eval_count'] += 1
        for key in accum_losses:
            add_point_to_plot(viz_dict, 'eval_' + key , viz_dict['eval_count'],
                              accum_losses[key])


if __name__ == '__main__':
    parser = argparse.ArgumentParser(description='Jigsaw Solving in PyTorch')
    # Model
    parser.add_argument('--model-name', type=str, default='noname',
                        help='give me a descriptive name for filenames')
    parser.add_argument('--model-type', type=str, default='inv-opt',
                        help='inv-opt, gumbel-sinkhorn, gaussian-sinhorn')
    parser.add_argument('--batch-size', type=int, default=100, metavar='N',
                        help='input batch size for training (default: 1)')
    parser.add_argument('--epochs', type=int, default=10, metavar='N',
                        help='number of epochs to train (default: 10)')
    parser.add_argument('--lr', type=float, default=0.01, metavar='LR',
                        help='learning rate (default: 0.01)')
    parser.add_argument('--momentum', type=float, default=0.5, metavar='M',
                        help='SGD momentum (default: 0.5)')
    parser.add_argument('--weight-decay', type=float, default=0, metavar='WD',
                        help='l2 regularization weight (default: 0)')
    parser.add_argument('--optimizer', type=str, default='adam', metavar='O',
                        help='which optimizer: sgd, adam (default: adam)')
    parser.add_argument('--cuda', action='store_true', default=False,
                        help='enables CUDA training')
    parser.add_argument('--seed', type=int, default=1, metavar='S',
                        help='random seed (default: 1)')
    parser.add_argument('--num-row-pieces', type=int, default=2, metavar='N',
                        help='number of pieces for the scrambled mnist dataset')
    parser.add_argument('--gaussian-var', type=float, default=0.5,
                        help='variance for the latent space')
    parser.add_argument('--no-margin', action='store_true', default=False,
                        help='disables the use of margin')
    parser.add_argument('--margin-scale', type=float, default=1,
                        help='constant to rescale the strong duality by')
    parser.add_argument('--resolution', type=int, default=56,
                        help='how much to upscale mnist images before chopping')
    # Evaluate, save, load, visualize
    parser.add_argument('--load', type=str,
                        help='load previously saved parameters')
    parser.add_argument('--save-dir', type=str,
                        help='save parameters, plots, evals to specified dir')
    parser.add_argument('--log-every', type=int, default=1, metavar='N',
                        help='batches to wait before logging training status')
    parser.add_argument('--eval-every', type=int, default=-1,
                        help='batches to wait until evaluating on valid set')
    parser.add_argument('--visualize', action='store_true', default=False,
                        help='enable visdom unscrambling visualizations')
    parser.add_argument('--samples_per_num', type=int, default=5,
                        help='number of samples for gumbel-sinkhorn')
    parser.add_argument('--n_iter_sinkhorn', type=int, default=10,
                        help='number of iteratins for sinkhorn operator')
    parser.add_argument('--temperature', type=int, default=1,
                        help='gumbel sinkhorn temperature')
    args = parser.parse_args()
    use_cuda = args.cuda and torch.cuda.is_available()

    np.random.seed(args.seed)
    torch.manual_seed(args.seed)

    # Load train/valid/test images.
    train_loaders = get_train_valid_loader(data_dir='../data',
                                           batch_size=args.batch_size,
                                           random_seed=args.seed,
                                           resolution=args.resolution)
    test_loader = get_test_loader(data_dir='../data',
                                  batch_size=args.batch_size,
                                  resolution=args.resolution)
    # Setup model & optimizer.
    model = setup_model(args)
    if use_cuda: model.cuda()
    if args.epochs > 0:
        optimizer = setup_optimizer(args, model.parameters())
        # Build directories
        if args.save_dir and not os.path.isdir(args.save_dir):
            os.makedirs(args.save_dir)
        fname_losses = '%s/%s_valid_losses.dat' % (args.save_dir, args.model_name)
        # Train if args.epochs > 0
        count_skipped_total = 0
        count_total_total = 0
        with open(fname_losses, 'w+') as f:
            viz_dict = {'viz_env':None, 'count':0, 'eval_count':0, 'valid_losses_file':f}
            f.write('l1_diff,l2_diff,kendal_tau,hamming,prop_wrong,prop_any_wrong\n')
            for epoch in range(1, args.epochs + 1):
                count_skipped, count_total = train(args, model, use_cuda, train_loaders,
                                                   optimizer, epoch, viz_dict)
                count_skipped_total += count_skipped
                count_total_total += count_total
            print('Done with training')
            print('Count skipped:', count_skipped_total,
                    'Percentage skipped:', count_skipped_total / count_total_total)
            f.close()
        exit()
    elif args.load:
        # Load model from checkpoint
        fname = args.load
        model.load_state_dict(torch.load(fname))

    if True:
        # Visualize the latent space
        num_points = 1000
        latent_data = []
        latent_labels = []
        for batch_idx, (data, target) in enumerate(train_loaders[1]):
            x_pieces, x_mixed_pieces, perms = utils.preprocess_batch(
                    data, args.num_row_pieces)
            x_pieces = Variable(x_pieces)
            x_mixed_pieces = Variable(x_mixed_pieces)
            if use_cuda:
                x_pieces = x_pieces.cuda()
                x_mixed_pieces = x_mixed_pieces.cuda()
            outs = model(x_mixed_pieces, perms)
            if outs is None:
                continue
            loss, acc, mean_pot, posterior_pot = outs
            for i in range(mean_pot.size(0)):
                for j in range(perms.size(1)):
                    latent_data.append(mean_pot[i][j].data)
                    latent_labels.append(perms[i][j:j+1])
            if len(latent_data) >= num_points:
                latent_data = latent_data[0:num_points]
                latent_labels = latent_labels[0:num_points]
                break
        latent_data = torch.stack(latent_data).numpy()
        latent_labels = torch.stack(latent_labels).numpy()
        epoch = args.load[args.load.rfind('_') + 1:-4]
        visualize_latent_space(latent_data, latent_labels,
                               figname='latent_space_' + str(epoch))


    if True:
        # Visualize some positive samples and some negative samples
        unsolved_x_pieces = []
        unsolved_x_mixed_pieces = []
        unsolved_mean_pot = []
        for batch_idx, (data, target) in enumerate(train_loaders[1]):
            x_pieces, x_mixed_pieces, perms = utils.preprocess_batch(
                    data, args.num_row_pieces)
            x_pieces = Variable(x_pieces)
            x_mixed_pieces = Variable(x_mixed_pieces)
            if use_cuda:
                x_pieces = x_pieces.cuda()
                x_mixed_pieces = x_mixed_pieces.cuda()
            outs = model(x_mixed_pieces, perms)
            if outs is None:
                continue
            loss, acc, mean_pot, posterior_pot = outs
            if acc['kendal_tau'] < 1:
                perms_pred = utils.matching(mean_pot.data)
                diff_perms = torch.abs(perms_pred - perms)
                any_wrong = torch.max(diff_perms, 1)[0]
                for i in range(any_wrong.size(0)):
                    if any_wrong[i]:
                        unsolved_x_pieces.append(x_pieces[i])
                        unsolved_x_mixed_pieces.append(x_mixed_pieces[i])
                        unsolved_mean_pot.append(mean_pot[i])
            if batch_idx == 0:
                visualize_jigsaw_solving(x_mixed_pieces, mean_pot.data,
                                         x_pieces=x_pieces,
                                         label='valid_batch')
            print('unsolved', len(unsolved_x_mixed_pieces))
            if len(unsolved_x_mixed_pieces) >= 10:
                unsolved_x_pieces = unsolved_x_pieces[1:10]
                unsolved_x_mixed_pieces = unsolved_x_mixed_pieces[1:10]
                unsolved_mean_pot = unsolved_mean_pot[1:10]
                break

        if len(unsolved_x_mixed_pieces):
            unsolved_x_pieces = torch.stack(unsolved_x_pieces)
            unsolved_x_mixed_pieces = torch.stack(unsolved_x_mixed_pieces)
            unsolved_mean_pot = torch.stack(unsolved_mean_pot)
            visualize_jigsaw_solving(unsolved_x_mixed_pieces,
                                     unsolved_mean_pot.data,
                                     x_pieces=unsolved_x_pieces,
                                     label='unsolved_valid_batch',
                                     display_mixed_pieces=True)

    if False:
        # Evaluate on train / valid / test
        print('Training dataset. Count:', len(train_loaders[0]))
        test(args, model, use_cuda, train_loaders[0], None, None, noise_factor=0)
        print('Validation dataset. Count:', len(train_loaders[1]))
        test(args, model, use_cuda, train_loaders[1], None, None, noise_factor=0)
        print('Test dataset. Count:', len(test_loader))
        test(args, model, use_cuda, test_loader, None, None, noise_factor=0)

