from __future__ import division, print_function

import torch
import itertools
import time
import numpy as np
import unittest
from torch.autograd import Variable

import utils
from visdom_utils import show_images

EQ_EPS = 1e-3
SEED = 1
torch.manual_seed(SEED)
np.random.seed(SEED)


class TestUtils(unittest.TestCase):
    def test_listperm_vs_matperm(self, verbose=False):
        perms = torch.LongTensor([[0,1,2],[0,2,1]])
        if verbose: print(perms)
        mats = utils.listperm2matperm(perms)
        if verbose: print(mats)
        perms_new = utils.matperm2listperm(mats)
        if verbose: print(perms_new)
        assert (perms - perms_new).abs().sum() == 0
        mats_new = utils.listperm2matperm(perms_new)
        assert (mats - mats_new).abs().sum() == 0

    def test_invert_listperm(self, verbose=False):
        perms = torch.LongTensor([[0,1,2],[0,2,1],[2,0,1]])
        if verbose: print(perms)
        inv_perm = utils.invert_listperm(perms)
        if verbose: print(inv_perm)
        assert (perms - utils.invert_listperm(utils.invert_listperm(perms))).abs().sum() == 0

    def test_image_vs_pieces(self, verbose=False):
        w = torch.arange(0, 16).view(1, 1, 4, 4)
        if verbose: print(w)
        p = utils.image2pieces(w, 2)
        if verbose: print(p)
        ww = utils.pieces2image(p)
        if verbose: print(ww)
        pp = utils.image2pieces(ww, 2)
        assert (w - ww).abs().sum() == 0
        assert (p - pp).abs().sum() == 0

    def test_scramble(self, verbose=False):
        p = torch.LongTensor([[1,2,0],[2,1,0]])
        if verbose: print(p + 1)
        w = torch.rand(2,3,2,100)
        if verbose: print(w.shape)
        new_w = utils.permute_batch_split(w, p)
        if verbose: print(new_w.shape)

    def test_gaussian_nll(self, verbose=False):
        # Test the nll in the reasonable variance range.
        for i in range(100):
            val = Variable(torch.rand(2, 10))
            mu = Variable(torch.rand(2, 10))
            logvar = Variable(torch.rand(2, 10))
            loss = utils.gaussian_nll_loss(val, mu, logvar, option=3)

        val = Variable(torch.rand(1))
        mu = Variable(torch.rand(1))

    def test_inverse_matching_one_object(self, verbose=True):
        ''' This tests many single-object instances at once and verifies that the
        output solution matches the mean of the latent distribution. '''
        n_examples = 1
        n_objects = 1
        listperm = utils.sample_permutations(n_examples, n_objects)
        if listperm.dim() == 1:
            listperm = listperm.unsqueeze(0)
        mu_one = torch.rand(n_objects * n_objects).view(1, n_objects, n_objects)
        mu = mu_one.repeat(listperm.size(0), 1, 1)
        sigma = torch.ones(listperm.size(0), n_objects, n_objects)
        tic = time.time()
        latent = utils.inverse_matching(listperm, mu, sigma, margin=True, verbose=verbose)
        # assert (latent - mu).abs().mean() < EQ_EPS, (latent - mu).abs().sum()
        if verbose: print('Time for %d inverse utils.matchings: %.2f' %
                (listperm.size(0), time.time() - tic))

        def evaluate_perm(listperm, latent):
            new_listperm = utils.matching(latent)
            def score_perm(listperm_local):
                matperm = utils.listperm2matperm(listperm_local)
                score_target = matperm.mul(latent)
                return score_target.sum(1).sum(1)
            score_change = (score_perm(listperm) - score_perm(new_listperm))
            score_change = score_change.abs().sum()
            score_change /= listperm.size(0)
            assert score_change < EQ_EPS, score_change
            perm_change = (listperm - new_listperm).abs().gt(0).sum(1).gt(0).sum()
            if verbose: print('Error %.2f/1' % (perm_change / listperm.size(0)))

        evaluate_perm(listperm, latent)

    def test_inverse_matching_two_objects(self, verbose=True):
        ''' This tests many single-object instances at once and verifies that the
        output solution matches the mean of the latent distribution. '''
        n_examples = 1
        n_objects = 2
        listperm = utils.sample_permutations(n_examples, n_objects)
        if listperm.dim() == 1:
            listperm = listperm.unsqueeze(0)
        mu_one = torch.rand(n_objects * n_objects).view(1, n_objects, n_objects)
        mu = mu_one.repeat(listperm.size(0), 1, 1)
        sigma = torch.ones(listperm.size(0), n_objects, n_objects)
        tic = time.time()
        latent = utils.inverse_matching(listperm, mu, sigma, margin=True, verbose=verbose)
        # assert (latent - mu).abs().mean() < EQ_EPS, (latent - mu).abs().sum()
        if verbose: print('Time for %d inverse utils.matchings: %.2f' %
                (listperm.size(0), time.time() - tic))

        def evaluate_perm(listperm, latent):
            new_listperm = utils.matching(latent)
            def score_perm(listperm_local):
                matperm = utils.listperm2matperm(listperm_local)
                score_target = matperm.mul(latent)
                return score_target.sum(1).sum(1)
            score_change = (score_perm(listperm) - score_perm(new_listperm))
            score_change = score_change.abs().sum()
            score_change /= listperm.size(0)
            assert score_change < EQ_EPS, score_change
            perm_change = (listperm - new_listperm).abs().gt(0).sum(1).gt(0).sum()
            if verbose: print('Error %.2f/1' % (perm_change / listperm.size(0)))

        evaluate_perm(listperm, latent)

    def test_inverse_matching_multiple_objects(self, verbose=False):
        ' This tests many instances at once and evaluates the speed. '
        n_examples = 20
        n_objects = 16
        listperm = utils.sample_permutations(n_examples, n_objects)
        if listperm.dim() == 1:
            listperm = listperm.unsqueeze(0)
        mu_one = torch.rand(n_objects * n_objects).view(1, n_objects, n_objects)
        mu = mu_one.repeat(listperm.size(0), 1, 1)
        sigma = torch.ones(listperm.size(0), n_objects, n_objects)
        tic = time.time()
        latent = utils.inverse_matching(listperm, mu, sigma, margin=True, verbose=verbose)
        if verbose: print('Time for %d inverse utils.matchings: %.2f' %
                (listperm.size(0), time.time() - tic))

        def evaluate_perm(listperm, latent):
            new_listperm = utils.matching(latent)
            def score_perm(listperm_local):
                matperm = utils.listperm2matperm(listperm_local)
                score_target = matperm.mul(latent)
                return score_target.sum(1).sum(1)
            score_change = (score_perm(listperm) - score_perm(new_listperm))
            score_change = score_change.abs().sum()
            score_change /= listperm.size(0)
            assert score_change < EQ_EPS, score_change
            perm_change = (listperm - new_listperm).abs().gt(0).sum(1).gt(0).sum()
            if verbose: print('Error %.2f/1' % (perm_change / listperm.size(0)))

        evaluate_perm(listperm, latent)

    def test_inverse_matching_random(self, verbose=False):
        ' This tests a single random instance. '
        n_examples = 1
        n_objects = 5
        listperm = torch.LongTensor([0, 1, 3, 2, 4]).view(1, n_objects)
        matperm = utils.listperm2matperm(listperm)
        mu = torch.rand(listperm.size(0), n_objects, n_objects)
        var = torch.ones(listperm.size(0), n_objects, n_objects)
        latent = utils.inverse_matching(listperm, mu, var, margin=True, verbose=verbose)
        best_listperm = utils.matching(latent)
        best_matperm = utils.listperm2matperm(best_listperm)
        if verbose: print(best_listperm)

    def test_inverse_matching_1(self, verbose=False):
        ' This is a case which failed in training in the past!'
        n_examples = 1
        n_objects = 4
        listperm = torch.LongTensor([3, 0, 2, 1]).view(1, n_objects)
        matperm = utils.listperm2matperm(listperm)
        mu = torch.Tensor([[-0.78005695, -0.9284065,  -0.96993285, -1.0949469 ],
                        [-0.70026857, -0.8828286,  -0.905834,   -1.0732366 ],
                        [-0.7025856,  -0.8412696,  -0.95031464, -1.0067744 ],
                        [-0.5604791,  -0.60978645, -0.87006724, -1.0070556 ]])
        mu = mu.view(listperm.size(0), n_objects, n_objects)
        var = torch.ones(listperm.size(0), n_objects, n_objects)
        latent = utils.inverse_matching(listperm, -mu, var, margin=True, verbose=verbose)
        best_listperm = utils.matching(latent)
        best_matperm = utils.listperm2matperm(best_listperm)
        if verbose: print(best_listperm)

    def test_inverse_matching_2(self, verbose=False):
        ''' This is a case which failed in training in the past!
            listperm [[0 2 3 1]]
            matperm [[[1. 0. 0. 0.]
                      [0. 0. 1. 0.]
                      [0. 0. 0. 1.]
                      [0. 1. 0. 0.]]]
            mu [[[ 0.21651447  0.16681585  0.03814166 -0.24366462]
                 [ 0.04884529 -0.00542776  0.164737   -0.21455228]
                 [ 0.21548542  0.01324733 -0.00964975 -0.10106112]
                 [ 0.32563877  0.05111212  0.08846912 -0.05531685]]]
            var [[[1. 1. 1. 1.]
                  [1. 1. 1. 1.]
                  [1. 1. 1. 1.]
                  [1. 1. 1. 1.]]]
        '''
        n_examples = 1
        n_objects = 4
        listperm = torch.LongTensor([0, 2, 3, 1]).view(1, n_objects)
        matperm = utils.listperm2matperm(listperm)
        mu = torch.Tensor([[ 0.21651447,  0.16681585,  0.03814166, -0.24366462],
                           [ 0.04884529, -0.00542776,  0.16473700, -0.21455228],
                           [ 0.21548542,  0.01324733, -0.00964975, -0.10106112],
                           [ 0.32563877,  0.05111212,  0.08846912, -0.05531685]])
        mu = mu.view(listperm.size(0), n_objects, n_objects)
        var = torch.ones(listperm.size(0), n_objects, n_objects)
        latent = utils.inverse_matching(listperm, mu, var, margin=True,
                margin_scale=1, verbose=True)
        best_listperm = utils.matching(latent)
        best_matperm = utils.listperm2matperm(best_listperm)
        if verbose: print(best_listperm)

    def test_sample_gumbel(self, verbose=False):
        shape = (10,10)
        v = utils.sample_gumbel(shape)
        assert v.shape == shape

    def test_sinkhorn(self, verbose=False):
        ''' Not much to here, guarantees about the output. Check for error. '''
        alpha = torch.arange(1, 10).view(3,3)
        log_alpha = torch.log(alpha)
        if verbose:
            print('alpha', alpha)
            print('log_alpha')
        new_alpha = utils.sinkhorn(log_alpha, n_iters=1)
        if verbose:
            print('new_alpha')
            print(utils.matching(new_alpha))
            print(utils.matching(alpha))

    def test_no_noise_sinkhorn(self, verbose=False):
        alpha = torch.arange(1, 10).view(3,3)
        log_alpha = torch.log(alpha)
        outputs = utils.random_sinkhorn(log_alpha, noise_factor=0, temp=0.01)
        new_alpha, log_alpha_w_noise = outputs
        target = utils.listperm2matperm(utils.matching(alpha))
        if verbose:
            print('new_alpha', torch.floor(new_alpha * 100) / 100)
            print('target', target)
        assert (target - new_alpha).abs().max() < 0.1

    def test_gumbel_sinkhorn(self, verbose=False):
        ''' Not much to here, guarantees about the output. Check for error. '''
        alpha = torch.arange(1, 10).view(3,3)
        log_alpha = torch.log(alpha)
        outputs = utils.random_sinkhorn(log_alpha, noise_factor=1, temp=0.01)
        new_alpha, log_alpha_w_noise = outputs
        if verbose:
            print('new_alpha', torch.floor(new_alpha * 100) / 100)
            print('target', utils.listperm2matperm(utils.matching(alpha)))

    def test_gaussian_sinkhorn(self, verbose=False):
        ''' Not much to here, guarantees about the output. Check for error. '''
        alpha = torch.arange(1, 10).view(3,3)
        log_alpha = torch.log(alpha)
        outputs = utils.random_sinkhorn(log_alpha, noise_factor=1, temp=0.01,
                                        distribution='gaussian')
        new_alpha, log_alpha_w_noise = outputs
        if verbose:
            print('new_alpha', torch.floor(new_alpha * 100) / 100)
            print('target', utils.listperm2matperm(utils.matching(alpha)))


    def test_hard_losses_encode_decode(self, verbose=True):
        file_name = 'demo/stata.png'
        x_img = utils.image_loader(file_name)
        num_row_pieces = 2
        n_objects = num_row_pieces ** 2
        x_pieces, x_mixed_pieces, perms = utils.preprocess_batch(
                x_img, num_row_pieces=num_row_pieces)
        mu_one = torch.rand(n_objects * n_objects).view(1, n_objects, n_objects)
        mu = mu_one.repeat(perms.size(0), 1, 1)
        sigma = torch.ones(perms.size(0), n_objects, n_objects)
        log_alpha = utils.inverse_matching(perms, mu, sigma)
        x_pieces_pred = utils.solve_puzzle(x_mixed_pieces, log_alpha)

        n_samples = 20
        log_alpha_w_noise = log_alpha.unsqueeze(1).repeat(1, n_samples, 1, 1)
        losses = utils.compute_hard_losses(x_pieces, x_mixed_pieces, perms,
                                           log_alpha_w_noise)
        print(losses)
        assert losses['prop_any_wrong'] < 1e-5
        assert losses['l1_diff'] < 1e-5
        assert losses['l2_diff'] < 1e-5
        assert losses['kendal_tau'] > 1 - 1e-5
        assert losses['prop_wrong'] < 1e-5
        assert losses['hamming'] < 1e-5

        # Display images
        #show_images(torch.cat([x_img,
        #                       utils.pieces2image(x_pieces),
        #                       utils.pieces2image(x_mixed_pieces),
        #                       utils.pieces2image(x_pieces_pred)], dim=0),
        #            save_png=True, title='demo/test_hard_losses')


if __name__ == '__main__':
    unittest.main()
