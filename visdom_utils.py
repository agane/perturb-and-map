import time
import torchvision.utils
from visdom import Visdom

def start_visdom():
    ''' Connect to visdom server and return handler '''
    viz = Visdom()
    startup_sec = 1
    while not viz.check_connection() and startup_sec > 0:
        time.sleep(0.1)
        startup_sec -= 0.1
    assert viz.check_connection(), 'No connection could be formed quickly'
    return viz

def update_plot(X, Y, viz=None, win=None, label=''):
    if viz is None:
        viz = start_visdom()
    if win is None:
        layout = dict(title=label, showlegend=True)
        win = viz.line(X=X, Y=Y, opts=layout)
    else:
        viz.line(X=X, Y=Y, win=win, update='append')
    return win, viz

def show_images(imgs, nrow=1, viz=None, win=None, save_png=False, title=None,
                caption=None):
    if viz is None:
        viz = start_visdom()
    imgs = (imgs - imgs.min()) / (imgs.max() - imgs.min())
    imgs = torchvision.utils.make_grid(imgs, nrow, pad_value=0.5)
    if save_png:
        assert title is not None, 'title is required'
        fname = title + '.png'
        torchvision.utils.save_image(imgs, fname, nrow)
    if win is None:
        win = viz.image(imgs, opts=dict(title=title, caption=caption))
    else:
        viz.image(imgs, opts=dict(title=title, caption=caption), win=win)

    return win
