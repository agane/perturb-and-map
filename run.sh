# MNIST-3x3

# evaluate (set if False to if True in the evaluation block)
CUDA_VISIBLE_DEVICES=0 python main.py --model-name thesismodeltwo_three --eval-every 500 --log-every 100 --save-dir thesismodelone_three --seed 54321 --num-row-pieces 3 --load thesismodeltwo_three/thesismodeltwo_three_5360.dat --epochs 0 > thesismodeltwo_three/evaluate.txt

# visualize (set if False to if True in the visualization blocks)
CUDA_VISIBLE_DEVICES=0 python main.py --model-name thesismodeltwo_three --eval-every 500 --log-every 100 --save-dir thesismodelone_three --seed 54321 --num-row-pieces 3 --load thesismodeltwo_three/thesismodeltwo_three_5360.dat --epochs 0 --batch-size 10

# MNIST-2x2 (I did not re-run this)

# train with margin
python main.py --model-name thesismodelone --eval-every 100 --log-every 100 --save-dir thesismodelone --seed 12345
python main.py --model-name thesismodeltwo --eval-every 100 --log-every 100 --save-dir thesismodeltwo --seed 54321

# same thing, no margin
python main.py --model-name thesismodeltwo --eval-every 100 --log-every 100 --save-dir thesismodeltwo --seed 54321 --margin-scale 0
