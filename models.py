import math
import torch
import torch.nn as nn
import torch.nn.functional as F
from torch.autograd import Variable

import utils
from visdom_utils import show_images

class PatchEmbeddingNet(nn.Module):
    def __init__(self, num_row_pieces, num_channels=1, img_sz=28,
                 num_conv_filters=64, maxpool_stride=2, hidden=64,
                 merge_channels=False, batch_norm=False, dropout=False):
        '''
        Args:
            merge_channels: If true, merge the patch embeddings, then predict
                the preference matrix from all jointly. If false, each patch
                embedding independently produces one preference matrix row.
        '''
        super(PatchEmbeddingNet, self).__init__()
        self.num_row_pieces = num_row_pieces
        self.maxpool_stride = maxpool_stride
        self.merge_channels = merge_channels
        self.batch_norm = batch_norm
        self.dropout = dropout
        assert merge_channels == False, 'not implemented yet'
        assert batch_norm == False, 'not implemented yet'
        assert dropout == False, 'not implemented yet'

        # Parametrized modules.
        num_pieces = num_row_pieces ** 2
        self.conv = nn.Conv2d(num_channels, num_conv_filters, kernel_size=5)
        if not self.merge_channels:
            self.fc_mu = nn.Linear(self.cnn_outsz(num_channels, img_sz), num_pieces)
        else:
            self.fc_single = nn.Linear(self.cnn_outsz(num_channels, img_sz), hidden)
            self.fc_concat = nn.Linear(hidden * num_pieces, hidden)
            self.fc_mu = nn.Linear(hidden, num_pieces * num_pieces)

    def cnn_outsz(self, num_channels, img_sz, verbose=False):
        ''' Compute the output size after applying the convolutional network
        to an input image with the given number of channels and size. '''
        piece_sz = img_sz // self.num_row_pieces
        piece = Variable(torch.zeros(1, num_channels, piece_sz, piece_sz))
        if verbose: print(piece.shape)
        conv_out = self.conv.forward(piece)
        if verbose: print(conv_out.shape)
        pool_out = F.relu(F.max_pool2d(conv_out, self.maxpool_stride))
        if verbose: print(pool_out.shape)
        return len(pool_out.view(-1))

    def forward(self, x):
        ''' Given scrambled pieces (bz x np x nc x psz x psz), compute the
        matrix of position preferences (bz x np x np) '''
        conv_inp = x.view(x.size(0) * x.size(1), -1, x.size(-2), x.size(-1))
        conv_out = self.conv(conv_inp)
        maxpool_out = F.relu(F.max_pool2d(conv_out, self.maxpool_stride))
        fc_inp = maxpool_out.view(maxpool_out.size(0), -1)
        return self.fc_mu(fc_inp).view(x.size(0), x.size(1), x.size(1))


class InverseOptimizationNet(nn.Module):
    def __init__(self, num_row_pieces, num_channels=1, img_sz=28,
                 num_conv_filters=64, maxpool_stride=2, hidden=64,
                 merge_channels=False, batch_norm=False, dropout=False,
                 gaussian_var=0.5, margin=True, margin_scale=1,
                 n_samples=20):
        super(InverseOptimizationNet, self).__init__()
        self.num_row_pieces = num_row_pieces
        self.gaussian_var = gaussian_var
        self.margin = margin
        self.margin_scale = margin_scale
        self.n_samples = n_samples
        self.patch_net = PatchEmbeddingNet(num_row_pieces, num_channels, img_sz,
                                           num_conv_filters, maxpool_stride,
                                           hidden, merge_channels, batch_norm,
                                           dropout)

    def evaluate(self, x_pieces, x_mixed_pieces, perms, noise_factor=1):
        n_samples = self.n_samples
        if noise_factor == 0:
            n_samples = 1   # No need to replicate if there's no noise
        mu = self.patch_net(x_mixed_pieces)
        log_alpha_w_noise = mu.data.unsqueeze(1).repeat(1, n_samples, 1, 1)
        if noise_factor > 0:
            noise = torch.Tensor(mu.size(0), n_samples, mu.size(1), mu.size(2))
            if mu.is_cuda:
                noise = noise.cuda()
            noise = noise.normal_(mean=0, std=math.sqrt(self.gaussian_var))
            log_alpha_w_noise += noise
        acc = utils.compute_hard_losses(None, x_mixed_pieces.data, perms,
                                        log_alpha_w_noise)
        return acc

    def forward(self, x_mixed_pieces, perms=None, verbose=False, noise_factor=1):
        ''' Forward through the model. Output the loss, accuracy, the
        computed potentials mean, the computed potentials posterior.

        Args:
            verbose: A binary variable indicating to print things.
        '''
        # Compute the embeddings per piece & resulting mean/var
        mu = self.patch_net(x_mixed_pieces)
        if perms is None:
            return None, None, mu, None

        var = Variable(torch.Tensor(mu.size()).fill_(self.gaussian_var))
        if mu.is_cuda:
            var = var.cuda()

        potentials = utils.inverse_matching(perms, mu.data, var.data,
                                            margin=self.margin,
                                            margin_scale=self.margin_scale,
                                            verbose=False,
                                            use_unnecessary_checks=False)
        if potentials is None:
            return None

        if mu.is_cuda:
            potentials = potentials.cuda()
        # Compute loss given estimated latent variables.
        batch_size = mu.size(0)
        if self.gaussian_var == 0.5:
            loss = F.mse_loss(mu.view(batch_size, -1),
                              Variable(potentials.view(batch_size, -1)))
        else:
            var_flat = var.view(batch_size, -1)
            normal = torch.distributions.Normal(mu.view(batch_size, -1),
                                                var.sqrt().view(batch_size, -1))
            loss = torch.mean(-normal.log_prob(
                        Variable(potentials.view(batch_size, -1))))
        # Compute kendal tau accuracy given samples
        log_alpha_w_noise = mu.data.unsqueeze(1).repeat(1, self.n_samples, 1, 1)
        if noise_factor > 1:
            noise = torch.Tensor(mu.size(0), self.n_samples, mu.size(1), mu.size(2))
            if mu.is_cuda:
                noise = noise.cuda()
            noise = noise.normal_(mean=0, std=math.sqrt(self.gaussian_var))
            log_alpha_w_noise += noise
        acc = utils.compute_hard_losses(None, x_mixed_pieces.data, perms,
                                        log_alpha_w_noise)

        # acc = utils.kendall_tau(perms_pred, perms)
        # acc = (sum(acc) / len(acc))[0]

        if verbose:
            # assert (perms - utils.matching(potentials)).abs().sum() < 1e-5

            # Show that the permutation stands out in the latent vars
            inv_perms = utils.invert_listperm(perms).contiguous()
            print('mu', mu.data[0].numpy())
            print('z', potentials[0].numpy())
            print('mu_ordered', utils.permute_batch_split(mu, inv_perms).data[0].numpy())
            print('z_ordered', utils.permute_batch_split(potentials, inv_perms).numpy())
            print('batch loss', loss.data[0], 'batch kendal-tau', acc)

        return loss, acc, mu, potentials


class RandomSinkhornNet(nn.Module):
    ''' Gumbel/Gaussian Sinkhorn Network mirroring the soriting code
    from here https://github.com/google/gumbel_sinkhorn/tree/master/sorting
    '''
    def __init__(self, num_row_pieces, num_channels=1, img_sz=28,
                 num_conv_filters=64, maxpool_stride=2, hidden=64,
                 merge_channels=False, batch_norm=False, dropout=False,
                 gaussian_var=0.5):
        super(RandomSinkhornNet, self).__init__()
        self.num_row_pieces = num_row_pieces
        self.gaussian_var = gaussian_var
        self.patch_net = PatchEmbeddingNet(num_row_pieces, num_channels, img_sz,
                                           num_conv_filters, maxpool_stride,
                                           hidden, merge_channels, batch_norm,
                                           dropout)

    def evaluate(self, x_pieces, x_mixed_pieces, perms):
        log_alpha = self.patch_net(x_mixed_pieces)
        log_alpha_w_noise = None # TODO
        kendal_tau = utils.compute_hard_losses(x_pieces, x_mixed_pieces, perms, log_alpha_w_noise)
        return kendal_tau, log_alpha

    def forward(self, x_mixed_pieces, x_pieces=None, perms=None, verbose=False):
        ''' Forward through the model. Output the loss, accuracy, the
        computed potentials mean, the computed potentials posterior.

        Args:
            verbose: A binary variable indicating to print things.
        '''
        # Compute the embeddings per piece & resulting mean/var.
        log_alpha = self.patch_net(x_mixed_pieces)
        # Sample using gumbel_sinkhorn from the constructed matrix log_alpha.
        soft_perm, log_alpha_w_noise = random_sinkhorn(
                log_alpha, self.temperature, self.samples_per_num,
                self.noise_factor, self.n_iter_sinkhorn,
                distribution=self.distribution, gaussian_var=self.gaussian_var)
        # Compute the l2 loss between correctly ordered and predicted pieces
        loss = utils.l2loss(x_pieces, x_mixed_pieces, soft_perms)
        # Compute kendal tau accuracy at the mean
        acc = utils.compute_hard_losses(x_pieces, x_mixed_pieces, perms, log_alpha_w_noise)
        return loss, acc
