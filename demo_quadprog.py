from __future__ import print_function

from numpy import array, dot
import os
import sys
sys.path.append('%s/../qpsolvers' % os.path.dirname(os.path.realpath(__file__)))
from qpsolvers import solve_qp

M = array([[1., 2., 0.], [-8., 3., 2.], [0., 1., 1.]])
P = dot(M.T, M)  # quick way to build a symmetric matrix
q = dot(array([3., 2., 3.]), M).reshape((3,))
G = array([[1., 2., 1.], [2., 0., 1.], [-1., 2., -1.]])
h = array([3., 2., -2.]).reshape((3,))

print("QP quadprog solution:", solve_qp(P, q, G, h, solver='quadprog'))
print("QP cvxopt solution:", solve_qp(P, q, G, h, solver='cvxopt'))

