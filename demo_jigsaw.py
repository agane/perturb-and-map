from __future__ import print_function, division
import numpy as np
import random
import torch
from PIL import Image
from torchvision import transforms
from torch.autograd import Variable

from visdom_utils import show_images
import utils

seed = random.randint(0, 1000)
torch.manual_seed(seed)
np.random.seed(seed)

file_name = 'demo/stata.png'

x_img = utils.image_loader(file_name, imsize=256)
x_pieces, x_mixed_pieces, perms = utils.preprocess_batch(x_img,
                                                         num_row_pieces=2)
show_images(torch.cat([x_img,
                       utils.pieces2image(x_pieces),
                       utils.pieces2image(x_mixed_pieces)], dim=0),
            save_png=True, title='demo/demo')

